<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tip extends Model
{
    protected $primaryKey = 'tid';
    protected $table = 'tips';
}
