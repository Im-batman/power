<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class payment extends Model
{
	protected  $guarded = [ ];
	protected  $primaryKey = 'pid';
	protected $hidden = ['flw_reference'];

	public function Customer() {
		return $this->belongsTo(customer::class,'cid','cid');
	}
}
