<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class card extends Model
{
	protected $primaryKey = 'cardid';
	protected $table = 'cards';

	public function Customer() {
		return $this->belongsTo(customer::class,'cid','cid');
	}

}
