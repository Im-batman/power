<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaction extends Model
{
    protected $primaryKey = 'tid';
    protected $table = 'transactions';

	public function Customer() {
		return $this->belongsTo(customer::class,'cid','cid');
    }

}
