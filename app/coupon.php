<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class coupon extends Model
{
    protected $primaryKey = 'cpid';
    protected $table = 'coupons';

	public function User() {
		return $this->belongsTo(User::class,'uid','uid');
    }
}
