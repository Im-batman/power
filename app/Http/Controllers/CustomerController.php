<?php

namespace App\Http\Controllers;

use App\customer;
use App\customerMeter;
use App\transaction;
use Illuminate\Http\Request;
use Psy\Util\Str;

class CustomerController extends Controller
{


	public function _construct(  ) {

		$this->middleware('authCustomer');
	}


	public function storedUser(  ) {
		if (session()->has('customer')){
			$user = session()->get('customer');
			return $user;
		}

	}


	public function getHome(Request $request  ) {
		$meters = customerMeter::where('cid',$this->storedUser()->cid)->get();
		$customer = session()->has('customer');
		$transactions = transaction::where('cid',$this->storedUser()->cid)->orderBy('created_at', 'desc')->paginate(4);

		return view('customer.home',[
			'transactions'=> $transactions,
			'customer' => $customer,
			'meters' => $meters

		]);


	}

// Auth
	public function getRegister(  ) {
		if (session()->has('customer')){
			$customer = session()->get('customer');
			return redirect('customer/home');
		}else{
			return view('customer/auth/register');

		}
	}


	public function registerCustomer(Request $request) {
//		return $request->all();

		$customers = customer::where('email',$request->email)->get();
		if (count($customers)>0){
			session()->flash('error', 'email already exists');
			return redirect()->back();
		}
//		try{
			$customer  = new customer();
			$customer->fname = $request->input('fname');
			$customer->sname = $request->input('sname');
			$customer->email = $request->input('email');
			$customer->password = bcrypt($request->input('password'));
			$customer->phone = $request->input('phone');
			$customer->birthday = $request->input('dob');
			$customer->referralCode = 'NU-'.str_random(10);
			$customer->role = $request->input('role');
			$customer->save();
			session()->flash('success','Account Created');
			return redirect('customer/login');

//		}catch (\Exception $e){
//			return $e;
//		}

	}


	public function getLogin(Request $request) {

//		return session()->get('customer');

		if (session()->has('customer')){
			$customer = session()->get('customer');
			return redirect('customer/home');
		}else{
			return view('customer/auth/login');
		}

	}


	public function loginCustomer(Request $request) {

//		return $request->all();

		$username = $request->input('username');
		$password = $request->input('password');

		if (!isset($username)) return 'username is required';
		if (!isset($password)) return 'password is required';

		$customer  = customer::where('email',$username)->first();
		if (count($customer) > 0){
			if (password_verify($password,$customer->password)){
				session()->put('customer',$customer);
				session()->flash('success', 'login Successful');
				return redirect('customer/home');

			}else{
				session()->flash('error','Email or password is wrong');
				return redirect()->back();
			}
		}else{
			$customer = customer::where('phone',$username)->first();
			if (count($username)>0){
				if(password_verify($username,$customer->password)){
					session()->put('customer', $customer);
					return redirect('customer/home');
				}else{
					session()->flash('error', 'phone number or password is wrong');
				}
			}else{
				session()->flash('error','Account does not exist');
				return redirect()->back();
			}

		}

	}


	public function logout( Request $request ) {
		session()->remove('customer');
		return redirect('customer/login');

	}



	// Meter

	public function getAddMeter( ) {
		$meters = customerMeter::where('cid',$this->storedUser()->cid)->get();
		return view('customer/meter/add',[
			'meters' => $meters
		]);
	}

	public function postAddMeter(Request $request  ) {

//		return $request->all();

		try{
			$meter = new customerMeter();
//			$meter->cid = session()->get('customer')->cid;
			$meter->cid = $this->storedUser()->cid;
			$meter->meterNo = $request->input('meterNo');
			$meter->meterAlias = $request->input('meterAlias'); //optional name for mater
			$meter->save();
			session()->flash('success', 'Meter Added Successfully');

			return redirect()->back();
		}catch (\Exception $e){
			return $e;
		}

	}

	public function manageMeters($cid) {
		try{
			$meters = customerMeter::where('cid',$cid)->get();
			return view('customer/meter/manage',[
				'meters' => $meters
			]);
		}catch (\Exception $e) {
			return $e;
		}

	}


	public function meterDetail(Request $request, $cmid ) {
		$meter = customerMeter::findorfail($cmid);
		return view('customer/meter/detail',[
			'meter' => $meter
		]);

	}

	public function editMeter( $cmid) {
		$meter = customerMeter::findorfail($cmid);
		return view('customer/meter/edit',[
			'meter' => $meter
		]);
	}

	public function updateMeter(Request $request,$cmid ) {

		try{
			$meter = customerMeter::findorfail($cmid);
			$meter->update($request->all());
			session()->flash('success','Update Successful');
			return redirect()->back();

		}catch (\Exception $e){
			return $e;
		}
	}

	public function deleteMeter(Request $request,$cmid ) {
		try{
			$meter = customerMeter::destroy($cmid);
			session('success','Deleted');
			return redirect()->back();
		}catch (\Exception $e){
			return $e;
		}
	}



	// unit purchases

	public function getPurchases(Request $request) {
		$sales = transaction::where('cid', $this->storedUser()->cid)->orderBy('created_at','desc')->paginate(5);
		return view('customer.transaction.manage',[
			'transactions' => $sales
		]);

	}




	// Transaction
	public function addTransaction(Request $request ) {

		$transaction = new  transaction();
		$transaction->cid = session()->get('customer')->cid;
		$transaction->amount = $request->input('amount');
		$transaction->meterNo = $request->input('meterNo');
		$transaction->pin = rand();
		$transaction->save();


//		return $this->transactionResult($transaction->tid);

		return redirect('customer/units/'.$transaction->tid);

		//get to nepa Api and buy power
		//update the transaction and text the customer their pin;

		//make a payment entry - sent to pending
		//update if successful and sent transaction to  completed
	}


	public function transactionResult($tid) {

		$transaction = transaction::find($tid);
		return view('customer/result',[
			'transaction' => $transaction
		]);

	}





	public function manageTransactions(Request $request ) {
		$transactions = transaction::where('cid',$this->storedUser()->cid)->orderBy('created_at', 'desc')->paginate(10);
//		return $transactions;
		return view('customer.transaction.manage',[
			'transactions' => $transactions
		]);
	}

	public function transactionDetails(Request $request, $tid ) {
		$transaction = transaction::findorfail($tid);
		return view('customer.transaction.details',[
			'transaction' => $transaction
		]);
	}







	//cards









}
