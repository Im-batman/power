<?php

namespace App\Http\Controllers;

use App\customer;
use App\customerMeter;
use App\setting;
use App\tip;
use App\transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{



	public function __construct()
	{
		$this->middleware('auth');
	}


    public function index()
    {
	    $transactions = transaction::orderBy('created_at','desc')->paginate(15);
	    $meters = customerMeter::all();
	    $customers = customer::all();
	    return view('admin.dashboard',[
	    	'transactions' => $transactions,
		    'meters' =>$meters,
		    'customers' =>$customers
	    ]);
    }



	public function customers(Request $request) {
    	if (Input::has('q')){
    		$term = Input::get('q');
    		$customers = customer::where('fname','like',"%$term%")->orwhere('sname',"like","%$term%")->paginate(10);
	    }else{
		    $customers = customer::orderBy('created_at','desc')->paginate(10);
	    }
//	    return $customers;
		return view('admin.customers.manage',[
			'customers' => $customers
		]);
    }


	public function customerDetail(Request $request, $cid ) {
    	$customer = customer::findorfail($cid);
    	return view('admin.customer.detail',[
    		'customer' =>$customer
	    ]);
    }


	public function transactions(Request $request) {
//		return Carbon::today();
//		return Carbon::now()->format('m');







		if (Input::has('time')){
			$time = Input::get('time');

			if ($time == 'month'){
				$month = Carbon::now()->format('m');
				$transactions = transaction::whereMonth('created_at',$month)->paginate(5);
//				return count($transactions);
//				return $transactions;
			}
			elseif ($time =='today'){
//				return Carbon::today();
				$transactions = transaction::where('created_at','>=',Carbon::today())->paginate(5);
//				return count($transactions);
//				return $transactions;
			}

		}elseif (Input::has('from') and Input::has('to')){

			$from = Input::get('from');
			$to = Input::get('to');

			$transactions = transaction::whereBetween('created_at',[$from,$to])->orderBy('created_at','desc')->paginate(5);

//			return $transactions;

		}elseif (Input::has('from')) {

			$from         = Input::get( 'from' );
			$transactions = transaction::where( 'created_at', '>', $from )->orderBy('created_at','desc')->paginate(5);

//			return $transactions;
		}else{
					$transactions = transaction::orderBy('created_at','desc')->paginate(5);

		}






		return view('admin.transactions.manage',[
			'transactions' => $transactions
		]);


    }

	public function transactionDetail( $tid ) {
    	$transaction = transaction::find($tid);
    	return view('admin.transaction.detail',[
    		'transaction' => $transaction
	    ]);


    }


	public function tips(Request $request  ) {
		$tips = tip::orderBy('created_at','desc')->paginate(10);
    	return view('admin/tips/manage',[
    		'tips' => $tips
	    ]);
    }

	public function tipDetail( $tpid ) {

    	$tip = tip::find($tpid);
    	return view('admin.tips.detail',[
    		'tip' => $tip
	    ]);

    }

	public function addTip(Request $request  ) {
    	$tip = new  tip();
    	$tip->uid = Auth::user()->uid;
    	$tip->title = $request->input('title');
    	$tip->content = $request->input('content');
    	$tip->save();
    	session()->flash('success','Tip Added');
    	return redirect()->back();
    }


	public function editTip($tpid) {
    	$tip = tip::find($tpid);
    	return view('admin.tip.edit',[
    		'tip' => $tip
	    ]);

    }


	public function deleteTip($tpid) {
		$tip = tip::destroy($tpid);
		session()->flash('success', 'Deleted');
		return redirect()->back();
	}




	//settings

	public function settings(Request $request  ) {
		$settings = setting::orderBy('created_at','desc')->paginate(10);
		return view('admin/settings/manage',[
			'settings' => $settings
		]);
	}

	public function settingDetail( $sid ) {

		$setting = setting::find($sid);
		return view('admin.settings.detail',[
			'setting' => $setting
		]);

	}

	public function addSetting(Request $request  ) {
		$tip = new  tip();
		$tip->uid = Auth::user()->uid;
		$tip->title = $request->input('title');
		$tip->content = $request->input('content');
		$tip->save();
		session()->flash('success','Tip Added');
		return redirect()->back();
	}


	public function editSetting($sid) {
		$setting = setting::find($sid);
		return view('admin.setting.edit',[
			'setting' => $setting
		]);

	}







	public function ExportPayments() {

		$payments = payment::all()->sortByDesc('created_at');

		//get settings
		$accessFeePercent = setting::where('name','accessFee')->get()->last()->value;
		$paymentSchedule = setting::where('name','paymentSchedule')->get()->last()->value;

		$modifiedPayments = collect();

		foreach ( $payments as $payment ) {
			try {$payment->Patient;} catch ( \Exception $exception ) {}
			try {$payment->Case;} catch ( \Exception $exception ) {}
			try {$payment->Case->Doctor;} catch ( \Exception $exception ) {}

			$maturityDate = Carbon::createFromFormat("Y-m-d H:i:s",$payment->Case->ended_at)
			                      ->addDays($paymentSchedule)->toDateTimeString();
			$consultationFee = $payment->amount;
			$accessFee       = $consultationFee * ( $accessFeePercent / 100 );
			$doctorsFee      = $consultationFee - $accessFee;

//    		 add new fields to our collection
			$payment = collect( $payment )->put( 'doctorsFee', $doctorsFee )
			                              ->put('maturityDate',$maturityDate)
			                              ->put( 'accessFee', $accessFee );

			$modifiedPayments->push( $payment );
		}



		$data =  $modifiedPayments;
		$serialNumber = 1;

		define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getProperties()->setCreator(Auth::user()->fname ." ". Auth::user()->sname)
		            ->setTitle("Doctap Payment Export - " . Carbon::now()->toDayDateTimeString() )
		            ->setSubject("Doctap Payments");

		$cell = 2;

		foreach ($data as $payment){

//			return $payment;
			try {

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'A1', "S/N" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'A' . $cell, $serialNumber );


				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'B1', "DATE AND TIME" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'B' . $cell, Carbon::createFromFormat("Y-m-d H:i:s",$payment['case']['ended_at'])->toDayDateTimeString() );

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'C1', "CASE ID" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'C' . $cell, $payment['casid'] );

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'D1', "PATIENT" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'D' . $cell, $payment['patient']['fname'] . " ". $payment['patient']['sname']);

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'E1', "DOCTOR" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'E' . $cell, $payment['case']['doctor']['fname'] . " ". $payment['case']['doctor']['sname'] );

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'F1', "ACCOUNT NAME" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'F' . $cell, $payment['case']['doctor']['accountName']);

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'G1', "ACCOUNT NUMBER" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'G' . $cell, $payment['case']['doctor']['accountNumber']);

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'H1', "ACCOUNT BANK" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'H' . $cell, $payment['case']['doctor']['accountBank']);

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'I1', "CONSULTATION FEE" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'I' . $cell, $payment['amount']);

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'J1', "DOCTOR'S FEE" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'J' . $cell, $payment['doctorsFee']);

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'K1', "ACCESS FEE" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'K' . $cell, $payment['accessFee']);

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'L1', "TRANSACTION ID" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'L' . $cell, $payment['others']);

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'M1', "MATURITY DATE" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'M' . $cell, $payment['maturityDate']);

				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'N1', "CASE STATUS" );
				$objPHPExcel->setActiveSheetIndex( 0 )->setCellValue( 'N' . $cell, $payment['case']['status']);


			} catch (\Exception $exception){}
			$cell++;
			$serialNumber++;
		}

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save("Doctap Payments Export_" .  Carbon::now()->toDateString() .".xlsx");

		return  response()->download("Doctap Payments Export_" .  Carbon::now()->toDateString() .".xlsx");

	}



}
