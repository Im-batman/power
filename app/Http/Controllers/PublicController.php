<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{

	public function about(  ) {
		return view('front.about');
	}

	public function feedback(  ) {
		return view('front.feedback');
	}

	public function faq(  ) {
		return view('front.faq');
	}
	
	
	
	
	
}
