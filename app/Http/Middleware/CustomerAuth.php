<?php

namespace App\Http\Middleware;

use Closure;

class CustomerAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

//	    $customer = session()->get('customer');

	    if (session()->get('customer') == null){
	    	return redirect('customer/login');
	    }


        return $next($request);
    }
}
