<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customerMeter extends Model
{
	protected $primaryKey = 'cmid';
	protected $table = 'customer_meters';
}
