<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{
	protected $primaryKey = 'cid';
	protected $table = 'customers';

	protected $hidden = ['password'];

	public function Payments() {
		return $this->hasMany(payment::class,'cid','cid');
	}

	public function Cards() {
		return $this->hasMany(card::class,'cid','cid');
	}

	public function Transactions() {
		return $this->hasMany(transaction::class,'cid','cid');
	}

	public function Meters() {
		return $this->hasMany(customerMeter::class,'cid','cid');
	}


}
