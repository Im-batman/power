<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('front.index');
});

Auth::routes();


//admin routes

Route::prefix('admin/')->group(function () {

	Route::get('home', 'HomeController@index')->name('home');

	Route::get('customers','HomeController@customers');
	Route::get('customer/{cid}/detail','HomeController@customerDetail');

	Route::get('transactions','HomeController@transactions');
//Route::get('admin/transactions','');





});














Route::get('about','PublicController@about');
Route::get('feedback','PublicController@feedback');
Route::get('faq','PublicController@faq');




//customer


Route::group(['middleware' => ['authCustomer']], function () {

	Route::prefix('customer/')->group(function (){

		Route::get('home','CustomerController@getHome');
		Route::get('add-meter','CustomerController@getAddMeter');
//meter
		Route::post('post-meter','CustomerController@postAddMeter');
		Route::get('delete-meter/{cmid}','CustomerController@deleteMeter');
		Route::get('customer/transactions','CustomerController@manageTransactions');
		Route::post('buy-power','CustomerController@addTransaction');
		Route::get('units/{tid}','CustomerController@transactionResult');
		Route::get('purchases','CustomerController@getPurchases');

	});

});





Route::prefix('customer/')->group(function (){

//Auth
	Route::get('logout','CustomerController@logout');
	Route::get('signup','CustomerController@getRegister');
	Route::post('register','CustomerController@registerCustomer');

	Route::get('login','CustomerController@getLogin');
	Route::post('signin','CustomerController@loginCustomer');


});




