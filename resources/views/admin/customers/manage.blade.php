@extends('admin.layout.base')

@section('content')


    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Dashboard</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <button class="btn btn-sm btn-outline-secondary">Share</button>
                <button class="btn btn-sm btn-outline-secondary">Export</button>
            </div>
            <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <span data-feather="calendar"></span>
                This week
            </button>
        </div>
    </div>

    {{--<canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>--}}

    <h2> Customers </h2>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>First name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Points</th>
                <th>Role</th>
                <th>Date</th>
            </tr>
            </thead>
            <tbody>
            @if(count($customers)>0)

				<?php $i = 1; ?>


                @foreach($customers as $customer)
                    <tr>
                        <td># <?php echo $i ?></td>
                        <td>{{$customer->fname}}</td>
                        <td>{{$customer->sname}}</td>
                        <td>{{$customer->email}}</td>
                        <td>{{$customer->phone}}</td>
                        <td>{{$customer->points}}</td>
                        <td>{{$customer->role}}</td>
                        <td>{{$customer->created_at}}</td>

                    </tr>
					<?php $i++?>
                @endforeach
            @else

                <h3 style="color: silver; text-align: center; margin-top: 30px;"> No Customers yet.</h3>

            @endif



            </tbody>
        </table>
    </div>


@endsection