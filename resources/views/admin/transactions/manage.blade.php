@extends('admin.layout.base')
@section('content')


    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Dashboard</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <button class="btn btn-sm btn-outline-secondary">Share</button>
                <button class="btn btn-sm btn-outline-secondary">Export</button>
            </div>



            <form class="form-inline" action="{{url('admin/transactions')}}" method="get">
                <div class="btn-group mr-2">
                    <button type="submit"  class="btn btn-sm btn-outline-success">
                        all
                    </button>
                </div>





                <div class="btn-group mr-2">
                    <button type="submit" name="time" value="today" class="btn btn-sm btn-outline-success">
                       Today
                    </button>
                    {{--<button class="btn btn-sm btn-outline-info">This week</button>--}}
                    {{--<button class="btn btn-sm btn-outline-danger">--}}
                    {{--<a href="{{url('/transactions?time=month')}}" > This Month </a>--}}
                    {{--</button>--}}
                </div>


                <button type="submit" name="time" value="month" class="btn btn-sm btn-outline-secondary dropdown-toggle">
                    <span data-feather="calendar"></span>
                    This Month
                </button>

            </form>



        </div>
    </div>

    {{--<canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>--}}

    <h2>Transactions</h2>

    <br>
    <form method="get" action="{{url('admin/transactions')}}">
    <div class="row">
           {{--<input type="hidden" name="_token" value="{{csrf_token()}}">--}}

            <div class="col-md-3 mb-3">
                <label for="cc-expiration">From</label>
                <input type="date" class="form-control" name="from" id="cc-expiration" placeholder="" required>
                <div class="invalid-feedback">
                    Expiration date required
                </div>
            </div>
            <div class="col-md-3 mb-3">
                <label for="cc-cvv">To</label>
                <input type="date" class="form-control" name="to" id="cc-cvv" placeholder=""  required>
                <div class="invalid-feedback">
                    Security code required
                </div>
            </div>


        <div class="col-md-3 mb-3" style="margin-top: 30px;">
            <button class="btn btn-outline-success" type="submit">Filter</button>

        </div>


    </div>
    </form>


    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Meter Number</th>
                <th>Amount</th>
                <th>Status</th>
                <th>Pin</th>
                <th>Date</th>
            </tr>
            </thead>
            <tbody>
            @if(count($transactions)>0)

				<?php $i = 1; ?>


                @foreach($transactions as $transaction)
                    <tr>
                        <td># <?php echo $i ?></td>
                        <td>{{$transaction->meterNo}} - {{$transaction->meterAlias}}</td>
                        <td>{{$transaction->amount}}</td>
                        <td>{{$transaction->status}}</td>
                        <td>{{$transaction->pin}}</td>
                        <td>{{$transaction->created_at->diffForHumans()}}</td>

                    </tr>
					<?php $i++?>
                @endforeach
            @else

                <h3 style="color: silver; text-align: center; margin-top: 30px;"> No Transactions</h3>

            @endif


            {{--<tr>--}}
                {{--<td>1,015</td>--}}
                {{--<td>sodales</td>--}}
                {{--<td>ligula</td>--}}
                {{--<td>in</td>--}}
                {{--<td>libero</td>--}}
                {{--<td>libero</td>--}}
            {{--</tr>--}}
            </tbody>
            {{$transactions->links()}}

        </table>
    </div>


@endsection
