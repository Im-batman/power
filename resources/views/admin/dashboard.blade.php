@extends('admin.layout.base')
@section('content')







    <div class="container" style="margin-top: -50px;">
        <div class="py-5 text-center">
            <img class="d-block mx-auto mb-4" src="../../assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
            <h2>Over view Of System</h2>
            {{--<p class="lead"> Store you meter numbers with us for easier purchase of units </p>--}}
        </div>

        <div class="row">
            <div class="col-md-4 order-md-2 mb-4">
                <h4 class="d-flex justify-content-between align-items-center mb-3">
                    <span class="text-muted"> Stats</span>
{{--                    <span class="badge badge-secondary badge-pill">{{count($meters)}}</span>--}}
                </h4>
                <ul class="list-group mb-3">

                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <h6 class="my-0">{{count($meters)}}</h6>
                                    <small class="text-muted">Meters</small>
                                </div>
                                {{--<span class="text-muted">edit</span>--}}
{{--                                <a href="{{url('customer/delete-meter/'.$meter->cmid)}}"> <span class="text-muted">delete</span></a>--}}
                            </li>


                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div>
                            <h6 class="my-0">{{count($customers)}}</h6>
                            <small class="text-muted">Customers</small>
                        </div>
                        {{--<span class="text-muted">edit</span>--}}
                        {{--                                <a href="{{url('customer/delete-meter/'.$meter->cmid)}}"> <span class="text-muted">delete</span></a>--}}
                    </li>


                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div>
                            <h6 class="my-0">{{count($transactions)}}</h6>
                            <small class="text-muted">Transactions</small>
                        </div>
                        {{--<span class="text-muted">edit</span>--}}
                        {{--                                <a href="{{url('customer/delete-meter/'.$meter->cmid)}}"> <span class="text-muted">delete</span></a>--}}
                    </li>




                    {{--<li class="list-group-item d-flex justify-content-between">--}}
                    {{--<span>Total (USD)</span>--}}
                    {{--<strong>$20</strong>--}}
                    {{--</li>--}}
                </ul>

                {{--<form class="card p-2">--}}
                {{--<div class="input-group">--}}
                {{--<input type="text" class="form-control" placeholder="Promo code">--}}
                {{--<div class="input-group-append">--}}
                {{--<button type="submit" class="btn btn-secondary">Redeem</button>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</form>--}}
            </div>
            <div class="col-md-8 order-md-1">
                <h4 class="mb-3">Recent Transactions</h4>
                <div class="table-responsive">
                    <table class="table table-striped table-sm">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Meter Number</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th>Pin</th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($transactions)>0)

				            <?php $i = 1; ?>


                            @foreach($transactions as $transaction)
                                <tr>
                                    <td># <?php echo $i ?></td>
                                    <td>{{$transaction->meterNo}}</td>
                                    <td>{{$transaction->amount}}</td>
                                    <td>{{$transaction->status}}</td>
                                    <td>{{$transaction->pin}}</td>
                                    <td>{{$transaction->created_at->diffForHumans()}}</td>

                                </tr>
					            <?php $i++?>
                            @endforeach
                        @else

                            <h3 style="color: silver; text-align: center; margin-top: 30px;"> Your haven't made any Purchases yet.</h3>

                        @endif


                        <tr>
                            <td>1,015</td>
                            <td>sodales</td>
                            <td>ligula</td>
                            <td>in</td>
                            <td>libero</td>
                            <td>libero</td>
                        </tr>
                        </tbody>

                        {{$transactions->links()}}
                    </table>
                </div>


            </div>
        </div>

        <footer class="my-5 pt-5 text-muted text-center text-small">
            <p class="mb-1">&copy; 2017-2018 Company Name</p>
            <ul class="list-inline">
                <li class="list-inline-item"><a href="#">Privacy</a></li>
                <li class="list-inline-item"><a href="#">Terms</a></li>
                <li class="list-inline-item"><a href="#">Support</a></li>
            </ul>
        </footer>
    </div>

    {{--<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">--}}

    {{--</main>--}}















    {{--<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">--}}
        {{--<h1 class="h2">Dashboard</h1>--}}
        {{--<div class="btn-toolbar mb-2 mb-md-0">--}}
            {{--<div class="btn-group mr-2">--}}
                {{--<button class="btn btn-sm btn-outline-secondary">Share</button>--}}
                {{--<button class="btn btn-sm btn-outline-secondary">Export</button>--}}
            {{--</div>--}}
            {{--<button class="btn btn-sm btn-outline-secondary dropdown-toggle">--}}
                {{--<span data-feather="calendar"></span>--}}
                {{--This week--}}
            {{--</button>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>--}}

    {{--<h2>Section title</h2>--}}
    {{--<div class="table-responsive">--}}
        {{--<table class="table table-striped table-sm">--}}
            {{--<thead>--}}
            {{--<tr>--}}
                {{--<th>#</th>--}}
                {{--<th>Meter Number</th>--}}
                {{--<th>Amount</th>--}}
                {{--<th>Status</th>--}}
                {{--<th>Pin</th>--}}
                {{--<th>Date</th>--}}
            {{--</tr>--}}
            {{--</thead>--}}
            {{--<tbody>--}}
            {{--@if(count($transactions)>0)--}}

				{{--<?php $i = 1; ?>--}}


                {{--@foreach($transactions as $transaction)--}}
                    {{--<tr>--}}
                        {{--<td># <?php echo $i ?></td>--}}
                        {{--<td>{{$transaction->meterNo}}</td>--}}
                        {{--<td>{{$transaction->amount}}</td>--}}
                        {{--<td>{{$transaction->status}}</td>--}}
                        {{--<td>{{$transaction->pin}}</td>--}}
                        {{--<td>{{$transaction->created_at}}</td>--}}

                    {{--</tr>--}}
					{{--<?php $i++?>--}}
                {{--@endforeach--}}
            {{--@else--}}

                {{--<h3 style="color: silver; text-align: center; margin-top: 30px;"> Your haven't made any Purchases yet.</h3>--}}

            {{--@endif--}}


            {{--<tr>--}}
                {{--<td>1,015</td>--}}
                {{--<td>sodales</td>--}}
                {{--<td>ligula</td>--}}
                {{--<td>in</td>--}}
                {{--<td>libero</td>--}}
                {{--<td>libero</td>--}}
            {{--</tr>--}}
            {{--</tbody>--}}
        {{--</table>--}}
    {{--</div>--}}


@endsection
