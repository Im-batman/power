<div class="container footer-container">
    <div class="foot-text">
        <img class="powered" src="{{url('front/images/icon_black.png')}}" alt="Powered by Quidizy" />
        <p class="copyright-text">Copyright © 2018 All Rights Reserved.</p>
    </div>
    <div class="social">
        <ul class="social-icons">
            <li><i class="fab fa-facebook-f"></i></li>
            <li><i class="fab fa-twitter"></i></li>
            <li><i class="fab fa-instagram"></i></li>
        </ul>
    </div>
</div>
