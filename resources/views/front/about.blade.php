<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NEPA Units</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="{{url('front/images/favicon.png')}}">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="{{url('front/css/about.css')}}" />

</head>

<body>
    <div id="app">
        <div class="about-info">
            <header id="about-home" class="about-home bg-image">
                <div class="overlay">
                    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                        <div class="container">
                            <a class="navbar-brand" href="{{url('/')}}"><img src="{{url('front/images/white.png')}}" alt="Nepa Unit Logo"></a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('about')}}">About</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('faq')}}">FAQ</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('feedback')}}">Feedback</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link login-link" href="login.html">Login</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </header>
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-sm-12 col-lg-7">
                        <div class="about-body">
                            <h4>About</h4>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                Perspiciatis quos recusandae rerum quia ut possimus optio, illum maiores officiis
                                pariatur
                                veniam ad est quo accusantium eligendi accusamus quis odio commodi.
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam molestias laboriosam
                                possimus
                                quo doloribus accusamus modi vero neque voluptatem. Corrupti consequatur commodi
                                quibusdam temporibus,
                                quis debitis modi nulla suscipit molestias.
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis quos recusandae
                                rerum quia
                                ut possimus optio, illum maiores officiis pariatur veniam ad est quo accusantium
                                eligendi accusamus
                                quis odio commodi.
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis quos recusandae
                                rerum quia
                                ut possimus optio, illum maiores officiis pariatur veniam ad est quo accusantium
                                eligendi accusamus
                                quis odio commodi.
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis quos recusandae
                                rerum
                                quia ut possimus optio, illum maiores officiis pariatur veniam ad est quo accusantium
                                eligendi
                                accusamus quis odio commodi.
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis quos recusandae
                                rerum
                                quia ut possimus optio, illum maiores officiis pariatur veniam ad est quo accusantium
                                eligendi
                                accusamus quis odio commodi.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-12 col-lg-5">
                        <div class="about-body-img">
                            <div class="image-holder">
                                <img src="{{url('front/images/manvector.png')}}" alt="Watch Our About Us video" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="container-fluid footer-section">
            <div class="container footer-container">
                <div class="foot-text">
                    <img class="powered" src="{{url('front/images/icon_black.png')}}" alt="Powered by Quidizy" />
                    <p class="copyright-text">Copyright © 2018 All Rights Reserved.</p>
                </div>
                <div class="social">
                    <ul class="social-icons">
                        <li><i class="fab fa-facebook-f"></i></li>
                        <li><i class="fab fa-twitter"></i></li>
                        <li><i class="fab fa-instagram"></i></li>
                    </ul>
                </div>
            </div>
        </footer>
    </div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/vue@2.5.17/dist/vue.js"></script>

</html>