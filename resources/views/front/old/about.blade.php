@extends('front.layout.front')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-7 col-sm-12 col-lg-7">
                <div class="about-body">
                    <h4>About</h4>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Perspiciatis quos recusandae rerum quia ut possimus optio, illum maiores officiis
                        pariatur
                        veniam ad est quo accusantium eligendi accusamus quis odio commodi.
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam molestias laboriosam
                        possimus
                        quo doloribus accusamus modi vero neque voluptatem. Corrupti consequatur commodi
                        quibusdam temporibus,
                        quis debitis modi nulla suscipit molestias.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis quos recusandae
                        rerum quia
                        ut possimus optio, illum maiores officiis pariatur veniam ad est quo accusantium
                        eligendi accusamus
                        quis odio commodi.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis quos recusandae
                        rerum quia
                        ut possimus optio, illum maiores officiis pariatur veniam ad est quo accusantium
                        eligendi accusamus
                        quis odio commodi.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis quos recusandae
                        rerum
                        quia ut possimus optio, illum maiores officiis pariatur veniam ad est quo accusantium
                        eligendi
                        accusamus quis odio commodi.
                    </p>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis quos recusandae
                        rerum
                        quia ut possimus optio, illum maiores officiis pariatur veniam ad est quo accusantium
                        eligendi
                        accusamus quis odio commodi.
                    </p>
                </div>
            </div>
            <div class="col-md-5 col-sm-12 col-lg-5">
                <div class="about-body-img">
                    <div class="image-holder">
                        <img src="{{url('front/images/manvector.png')}}" alt="Watch Our About Us video" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection