@extends('front.layout.front')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-lg-8 offset-md-2">
                <div class="faq-body">
                    <h4>FAQs</h4>
                    <div id="accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne"
                                            aria-expanded="true" aria-controls="collapseOne">
                                        Question 1
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                 data-parent="#accordion">
                                <div class="card-body">
                                    <p class="card-text">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus
                                        terry
                                        richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard
                                        dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
                                        moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                        nulla
                                        assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer
                                        labore
                                        wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur
                                        butcher
                                        vice lomo. Leggings occaecat craft beer farm-to-table, raw denim
                                        aesthetic
                                        synth nesciunt you probably haven't heard of them accusamus labore
                                        sustainable VHS.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo"
                                            aria-expanded="false" aria-controls="collapseTwo">
                                        Question 2
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    <p class="card-text">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus
                                        terry
                                        richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard
                                        dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
                                        moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                        nulla
                                        assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer
                                        labore
                                        wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur
                                        butcher
                                        vice lomo. Leggings occaecat craft beer farm-to-table, raw denim
                                        aesthetic
                                        synth nesciunt you probably haven't heard of them accusamus labore
                                        sustainable VHS.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree"
                                            aria-expanded="false" aria-controls="collapseThree">
                                        Question 3
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <p class="card-text">
                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus
                                        terry
                                        richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard
                                        dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
                                        moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                        nulla
                                        assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer
                                        labore
                                        wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur
                                        butcher
                                        vice lomo. Leggings occaecat craft beer farm-to-table, raw denim
                                        aesthetic
                                        synth nesciunt you probably haven't heard of them accusamus labore
                                        sustainable VHS.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection