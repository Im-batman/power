@extends('front.layout.front')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-lg-6 offset-md-3">
                <div class="feedback-body">
                    <h4>Feedback Form</h4>
                    <span class="small">Drop your feedback and we will get back to you as soon as possible.</span>
                    <form id="form" @submit.prevent="onSubmit">
                        <p>
                            <input placeholder="Name:" required class="form-control" type="text" name="name" id="name"
                                   v-model="feedback.name">
                        </p>
                        <p>
                            <input placeholder="Email Address:" required class="form-control" type="email" name="email"
                                   id="email" v-model="feedback.email">
                        </p>
                        <p>
                            <input placeholder="Phone Number:" required class="form-control" type="text" name="phone"
                                   id="phone" v-model="feedback.phone">
                        </p>
                        <p>
                            <input placeholder="Subject:" required class="form-control" type="text" name="subject"
                                   id="subject" v-model="feedback.subject">
                        </p>
                        <p>
                            <textarea placeholder="Your Message Here:" class="form-control" rows="7" v-model="feedback.message"></textarea>
                        </p>
                        <p>
                            <button type="submit" class="btn btn-primary" value="Submit">Send</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @endsection