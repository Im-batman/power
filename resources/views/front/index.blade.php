<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NEPA Units</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="{{url('front/images/favicon.png')}}">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="{{url('front/css/index.css')}}" />

</head>

<body>
    <div class="welcome" id="app">
        <header id="main-home" class="my-home bg-image">
            <div class="home-overlay">
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                    <div class="container">
                        <a class="navbar-brand" href="{{url('/')}}"><img src="{{url('front/images/white.png')}}" alt="Nepa Unit Logo"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('about')}}">About</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('faq')}}">FAQ</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('feedback')}}">Feedback</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link login-link" href="{{url('customer/login')}}">Login</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-lg-8 home-header">
                            <p class="home-header-p">
                                Get out of the dark,
                            </p>
                            <p class="home-header-p2">Power up with NepaUnits</p>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-holder">
                                <div class="card">
                                    <div>
                                        <div class="card-header">
                                            <div class="nav nav-tabs card-header-tabs" id="nav-tab" role="tablist">
                                                <a class="nav-item nav-link active" id="nav-prepaid-tab" data-toggle="tab"
                                                    href="#nav-prepaid" role="tab" aria-controls="nav-prepaid"
                                                    aria-selected="true">Prepaid</a>
                                                <a class="nav-item nav-link" id="nav-postpaid-tab" data-toggle="tab"
                                                    href="#nav-postpaid" role="tab" aria-controls="nav-postpaid"
                                                    aria-selected="false">Postpaid</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-content" id="nav-tabContent">
                                        <div class="tab-pane card-body fade show active" id="nav-prepaid" role="tabpanel"
                                            aria-labelledby="nav-prepaid-tab">
                                            <form id="form" @submit.prevent="onSubmit">
                                                <p>
                                                    <select name="" id="" class="form-control">
                                                        <option value="Abuja">Abuja</option>
                                                        <option value="Lagos">Lagos</option>
                                                        <option value="PHC">Port Harcourt</option>
                                                    </select>
                                                </p>
                                                <p>
                                                    <input type="number" placeholder="Meter Number:" class="form-control">
                                                </p>
                                                <p>
                                                    <input type="number" placeholder="Phone Number:" class="form-control">
                                                </p>
                                                <p>
                                                    <input type="email" placeholder="Email Address:" class="form-control">
                                                </p>
                                                <p>
                                                    <input type="number" placeholder="Amount:" class="form-control">
                                                </p>
                                                <div class="">
                                                    <button :disabled="errors.any()" type="submit" class="btn btn-primary"
                                                        value="Recharge">Buy Now</button>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane card-body fade" id="nav-postpaid" role="tabpanel"
                                            aria-labelledby="nav-postpaid-tab">
                                            <form id="form2" @submit.prevent="onSubmit">
                                                <p>
                                                    <select name="" id="" class="form-control">
                                                        <option value="Abuja">Abuja</option>
                                                        <option value="Lagos">Lagos</option>
                                                        <option value="PHC">Port Harcourt</option>
                                                    </select>
                                                </p>
                                                <p>
                                                    <input type="number" placeholder="Metre Number:" class="form-control">
                                                </p>
                                                <p>
                                                    <input type="number" placeholder="Phone No:" class="form-control">
                                                </p>
                                                <p>
                                                    <input type="email" placeholder="Email Address:" class="form-control">
                                                </p>
                                                <p>
                                                    <input type="number" placeholder="Amount:" class="form-control">
                                                </p>
                                                <div class="">
                                                    <button :disabled="errors.any()" type="submit" class="btn btn-primary"
                                                        value="Recharge">Buy Now</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/vue@2.5.17/dist/vue.js"></script>

</html>