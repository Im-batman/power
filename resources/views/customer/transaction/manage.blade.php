@extends('customer.base')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Dashboard</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <button class="btn btn-sm btn-outline-secondary">Share</button>
                <button class="btn btn-sm btn-outline-secondary">Export</button>
            </div>
            <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <span data-feather="calendar"></span>
                This week
            </button>
        </div>
    </div>

    {{--<canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>--}}

    <h2>Section title</h2>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Meter Number</th>
                <th>Amount</th>
                <th>Status</th>
                <th>Pin</th>
                <th>Date</th>
            </tr>
            </thead>
            <tbody>
            @if(count($transactions)>0)
                <?php $i = 1; ?>

            @foreach($transactions as $transaction)
            <tr>
                <td># <?php echo $i ?></td>
                <td>{{$transaction->meterNo}}</td>
                <td>{{$transaction->amount}}</td>
                <td>{{$transaction->status}}</td>
                <td>{{$transaction->pin}}</td>
                <td>{{$transaction->created_at->diffForHumans()}}</td>

            </tr>
                <?php $i++?>
            @endforeach
            @else

                <h3 style="color: silver; text-align: center; margin-top: 30px;"> Your haven't made any Purchases yet.</h3>

            @endif


            {{--<tr>--}}
                {{--<td>1,015</td>--}}
                {{--<td>sodales</td>--}}
                {{--<td>ligula</td>--}}
                {{--<td>in</td>--}}
                {{--<td>libero</td>--}}
                {{--<td>libero</td>--}}
            {{--</tr>--}}
            </tbody>
        </table>
    </div>


@endsection