@extends('customer.base')

@section('content')


    <div class="container" style="margin-top: 100px;">
        <div class="py-5 text-center">
            <img class="d-block mx-auto mb-4" src="../../assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
            <h2 style="color: #2d995b">{{$transaction->pin}} </h2>
            <p class="lead"> Unit Purchase of <h4 style="color: #2d995b">{{$transaction->amount}}</h4> successful. Thank you! </p>
        </div>


        <footer class="my-5 pt-5 text-muted text-center text-small">
            <p class="mb-1">&copy; 2017-2018 Company Name</p>
            <ul class="list-inline">
                <li class="list-inline-item"><a href="#">Privacy</a></li>
                <li class="list-inline-item"><a href="#">Terms</a></li>
                <li class="list-inline-item"><a href="#">Support</a></li>
            </ul>
        </footer>
    </div>

    {{--<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">--}}

    {{--</main>--}}


@endsection