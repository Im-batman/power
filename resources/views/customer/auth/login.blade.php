

{{--<!DOCTYPE html>--}}
{{--<html>--}}

{{--<head>--}}
    {{--<meta charset="utf-8" />--}}
    {{--<meta http-equiv="X-UA-Compatible" content="IE=edge">--}}
    {{--<title>NEPA Units</title>--}}
    {{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}
    {{--<link rel="icon" type="image/png" href="{{url('front/images/favicon.png')}}">--}}
    {{--<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"--}}
          {{--crossorigin="anonymous">--}}
    {{--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"--}}
          {{--crossorigin="anonymous">--}}
    {{--<link rel="stylesheet" type="text/css" media="screen" href="{{url('front/css/login.css')}}" />--}}

{{--</head>--}}

{{--<body>--}}
{{--<div id="app">--}}
    {{--<div class="about-info">--}}
        {{--<header id="about-home" class="about-home bg-image">--}}
            {{--@include('front.layout.nav')--}}
        {{--</header>--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-6 col-sm-12 col-lg-6 offset-md-3">--}}
                    {{--<div class="login-body">--}}
                        {{--<h4>Login Form</h4>--}}
                        {{--<span class="small">If You are new here, Click <a href="{{url('customer/signup')}}"> Here </a> to register.</span>--}}
                        {{--<form method="post" action="{{url('customer/signin')}}" id="form" @submit.prevent="onSubmit">--}}
                            {{--{{csrf_field()}}--}}
                            {{--<p>--}}
                                {{--<input placeholder="Email Address:" required class="form-control" type="email" name="username"--}}
                                       {{--id="email" v-model="login.email">--}}
                            {{--</p>--}}
                            {{--<p>--}}
                                {{--<input placeholder="Password:" required class="form-control" type="password" name="password"--}}
                                       {{--id="password" v-model="login.password">--}}
                            {{--</p>--}}
                            {{--<p>--}}
                                {{--<button type="submit" class="btn btn-primary" value="Submit">Login</button>--}}
                            {{--</p>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<footer class="container-fluid footer-section">--}}
        {{--@include('front.layout.footer')--}}
    {{--</footer>--}}
{{--</div>--}}
{{--</body>--}}
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>--}}
{{--<script src="https://unpkg.com/vue@2.5.17/dist/vue.js"></script>--}}

{{--</html>--}}





@extends('front.layout.front')
@section('content')


    <div class="row">
        <div class="col-md-6 col-sm-12 col-lg-6 offset-md-3">
            <div class="login-body">
                <h4>Login Form</h4>
                <span class="small">If You are new here, Click <a href="{{url('customer/signup')}}"> Here </a> to register.</span>
                <form method="post" action="{{url('customer/signin')}}" id="form" @submit.prevent="onSubmit">
                    {{csrf_field()}}
                    <p>
                        <input placeholder="Email Address:" required class="form-control" type="email" name="username"
                               id="email" v-model="login.email">
                    </p>
                    <p>
                        <input placeholder="Password:" required class="form-control" type="password" name="password"
                               id="password" v-model="login.password">
                    </p>
                    <p>
                        <button type="submit" class="btn btn-primary" value="Submit">Login</button>
                    </p>
                </form>
            </div>
        </div>
    </div>


@endsection