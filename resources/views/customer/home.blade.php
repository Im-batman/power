@extends('customer.base')

@section('content')

    {{--<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">--}}
        {{--<h1 class="h2">Home</h1>--}}
        {{--<div class="btn-toolbar mb-2 mb-md-0">--}}
            {{--<div class="btn-group mr-2">--}}
                {{--<button class="btn btn-sm btn-outline-secondary">Share</button>--}}
                {{--<button class="btn btn-sm btn-outline-secondary">Export</button>--}}
            {{--</div>--}}
            {{--<button class="btn btn-sm btn-outline-secondary dropdown-toggle">--}}
                {{--<span data-feather="calendar"></span>--}}
                {{--This week--}}
            {{--</button>--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class="container" style="margin-top: -120px;">
        <div class="py-5 text-center">
            <img class="d-block mx-auto mb-4" src="../../assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
            {{--<h2>Buy Units</h2>--}}
            {{--<p class="lead"> Store you meter numbers with us for easier purchase of units </p>--}}
        </div>

        <div class="row">
            <div class="col-md-8 order-md-1">
                <h4 class="mb-3">Buy Units</h4>
                <form method="post" action="{{url('customer/buy-power')}}" class="needs-validation" novalidate>

                    {{csrf_field()}}

                    <div class="mb-3">
                        <label for="address">Meter</label>
                        <select name="meterNo" class="custom-select d-block w-100" id="state" required>
                            <option disabled selected> Pick a meter</option>
                            @foreach($meters as $meter)

                                <option value="{{$meter->meterNo}}">{{$meter->meterAlias ." - ". $meter->meterNo}}</option>

                            @endforeach
                        </select>
                        <div class="invalid-feedback">
                            Please provide a valid state.
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="address2">Amount <span class="text-muted">(500 and Above)</span></label>
                        <input type="text" class="form-control" id="address2" name="amount" placeholder="Enter amount">
                    </div>

                        <label>Service charge of N100 will be added</label>

                    </br>
                    </br>

                    {{--<hr class="mb-4">--}}
                    {{--<div class="custom-control custom-checkbox">--}}
                        {{--<input type="checkbox" class="custom-control-input" id="same-address">--}}
                        {{--<label class="custom-control-label" for="same-address">Shipping address is the same as my billing address</label>--}}
                    {{--</div>--}}
                    {{--<div class="custom-control custom-checkbox">--}}
                        {{--<input type="checkbox" class="custom-control-input" id="save-info">--}}
                        {{--<label class="custom-control-label" for="save-info">Save this information for next time</label>--}}
                    {{--</div>--}}
                    <hr class="mb-4">

                    <h4 class="mb-3">Payment</h4>
                    <div class="row">
                        <div class="col-md-5 mb-3">
                            <label for="country">Pick a Card (For Cards saved with us)</label>
                            <select class="custom-select d-block w-100" id="country" required>
                                <option value="">Choose...</option>
                                <option>United States</option>
                            </select>
                            <div class="invalid-feedback">
                                Please select a valid country.
                            </div>
                        </div>

                    </div>


                    <div class="row">
                        <div class="col-md-3 mb-3">
                            <label for="cc-expiration">OR</label>
                            {{--<input type="text" class="form-control" id="cc-expiration" placeholder="" required>--}}
                            {{--<div class="invalid-feedback">--}}
                                {{--Expiration date required--}}
                            {{--</div>--}}
                        </div>
                        {{--<div class="col-md-3 mb-3">--}}
                            {{--<label for="cc-cvv">CVV</label>--}}
                            {{--<input type="text" class="form-control" id="cc-cvv" placeholder="" required>--}}
                            {{--<div class="invalid-feedback">--}}
                                {{--Security code required--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to pay</button>
                </form>
            </div>
        </div>

        <footer class="my-5 pt-5 text-muted text-center text-small">
            <p class="mb-1">&copy; 2017-2018 Company Name</p>
            <ul class="list-inline">
                <li class="list-inline-item"><a href="#">Privacy</a></li>
                <li class="list-inline-item"><a href="#">Terms</a></li>
                <li class="list-inline-item"><a href="#">Support</a></li>
            </ul>
        </footer>
    </div>





    {{--<canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>--}}

    <h2>Section title</h2>
    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>#</th>
                <th>Meter Number</th>
                <th>Amount</th>
                <th>Status</th>
                <th>Pin</th>
                <th>Date</th>
            </tr>
            </thead>
            <tbody>
            @if(count($transactions)>0)
				<?php $i = 1; ?>

                @foreach($transactions as $transaction)
                    <tr>
                        <td># <?php echo $i ?></td>
                        <td>{{$transaction->meterNo}}</td>
                        <td>{{$transaction->amount}}</td>
                        <td>{{$transaction->status}}</td>
                        <td>{{$transaction->pin}}</td>
                        <td>{{$transaction->created_at}}</td>

                    </tr>
					<?php $i++?>
                @endforeach
            @else

                <h3 style="color: silver; text-align: center; margin-top: 30px;"> Your haven't made any Purchases yet.</h3>

            @endif


            <tr>
                <td>1,015</td>
                <td>sodales</td>
                <td>ligula</td>
                <td>in</td>
                <td>libero</td>
                <td>libero</td>
            </tr>
            </tbody>
        </table>
    </div>


@endsection