@extends('customer.base')
@section('content')

    <div class="container" style="margin-top: -50px;">
        <div class="py-5 text-center">
            <img class="d-block mx-auto mb-4" src="../../assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
            <h2>Add a  Meter Number</h2>
            <p class="lead"> Store you meter numbers with us for easier purchase of units </p>
        </div>

        <div class="row">
            <div class="col-md-4 order-md-2 mb-4">
                <h4 class="d-flex justify-content-between align-items-center mb-3">
                    <span class="text-muted">Your Meters</span>
                    <span class="badge badge-secondary badge-pill">{{count($meters)}}</span>
                </h4>
                <ul class="list-group mb-3">
                    @if(count($meters)>0)

                        @foreach($meters as $meter)
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <h6 class="my-0">{{$meter->meterNo}}</h6>
                                    <small class="text-muted">{{$meter->meterAlias}}</small>
                                </div>
                                {{--<span class="text-muted">edit</span>--}}
                               <a href="{{url('customer/delete-meter/'.$meter->cmid)}}"> <span class="text-muted">delete</span></a>
                            </li>

                        @endforeach
                        @else

                        @endif


                    {{--<li class="list-group-item d-flex justify-content-between">--}}
                        {{--<span>Total (USD)</span>--}}
                        {{--<strong>$20</strong>--}}
                    {{--</li>--}}
                </ul>

                {{--<form class="card p-2">--}}
                    {{--<div class="input-group">--}}
                        {{--<input type="text" class="form-control" placeholder="Promo code">--}}
                        {{--<div class="input-group-append">--}}
                            {{--<button type="submit" class="btn btn-secondary">Redeem</button>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</form>--}}
            </div>
            <div class="col-md-8 order-md-1">
                <h4 class="mb-3">Billing address</h4>
                <form class="needs-validation" method="post" action="{{url('customer/post-meter')}}">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="firstName"> Meter Number</label>
                            <input style="color: silver" type="text" class="form-control" id="firstName" placeholder="11 digits of meter" name="meterNo"  required>
                            <div class="invalid-feedback">
                                Valid Meter Number is required.
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="lastName">Meter Name</label>
                            <input type="text" class="form-control" id="lastName" placeholder="meter alias e.g My House" name="meterAlias"  >
                            <div class="invalid-feedback">
                                Valid last name is required.
                            </div>
                        </div>
                    </div>


                    <hr class="mb-4">
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Add </button>
                </form>
            </div>
        </div>

        <footer class="my-5 pt-5 text-muted text-center text-small">
            <p class="mb-1">&copy; 2017-2018 Company Name</p>
            <ul class="list-inline">
                <li class="list-inline-item"><a href="#">Privacy</a></li>
                <li class="list-inline-item"><a href="#">Terms</a></li>
                <li class="list-inline-item"><a href="#">Support</a></li>
            </ul>
        </footer>
    </div>

    {{--<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">--}}

    {{--</main>--}}



@endsection
