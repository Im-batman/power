<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

    	Schema::create('customers', function(Blueprint $table){
    		$table->increments('cid');
    		$table->string('fname');
    		$table->string('sname');
    		$table->string('email',191)->unique();
    		$table->string('password');
    		$table->string('phone');
    		$table->string('birthday')->nullable();
    		$table->integer('points')->default(0);
    		$table->string('referralCode',191)->unique();
		    $table->enum('role',['customer','reseller']);  //customer,reseller
		    $table->integer('balance')->default(0);
    		$table->timestamps();
    	});

    	Schema::create('recurrent', function(Blueprint $table){
    	    $table->increments('rid');
    	    $table->integer('cid');
    	    $table->integer('cardid');
    	    $table->integer('meterNo');
    	    $table->integer('amount');
    	    $table->integer('schedule'); // in days
		    $table->timestamps();
    	});

    	Schema::create('payments', function(Blueprint $table){
    	    $table->increments('pid');
    	    $table->integer('cid');
		    $table->enum('status',['initiated','paid','completed','failed'])->default('initiated');
		    $table->string('reference',191)->unique();
		    $table->string('flw_reference',191)->unique()->nullable();
		    $table->timestamps();
	    });

    	Schema::create('tips', function(Blueprint $table){
    		$table->increments('tpid');
    		$table->integer('uid');
    		$table->string('title');
    		$table->string('content',5000);
    		$table->timestamps();

    	});
    	
    	Schema::create('customer_meters', function(Blueprint $table){
    		$table->increments('cmid');
    		$table->integer('cid');
    		$table->integer('meterNo');
    		$table->string('meterAlias');
    		$table->timestamps();
    	});

    	Schema::create('transactions', function(Blueprint $table){
    		$table->increments('tid');
		    $table->integer('cid');
    		$table->integer('amount');
    		$table->integer('meterNo');
		    $table->enum('status',['pending','completed','failed'])->default('pending');
		    $table->integer('pin')->nullable();
    		$table->timestamps();
    	});

	    Schema::create('cards', function(Blueprint $table){
		    $table->increments('cardid');
		    $table->integer('cid');
		    $table->string('authCode');
		    $table->string('cardNumber');
		    $table->string('cardType');
		    $table->timestamps();
	    });


	    Schema::create('settings', function (Blueprint $table) {
		    $table->increments('sid');
		    $table->integer('uid');
		    $table->string('name');
		    $table->string('value');
		    $table->timestamps();
	    });

	    Schema::create('coupons', function(Blueprint $table){
	        $table->increments('cpid');
	        $table->integer('uid');
	        $table->string('type');
	        $table->integer('discount');
	        $table->timestamps();
	    });


	    Schema::create('users', function (Blueprint $table) {
		    $table->increments('uid');
		    $table->string('name');
		    $table->string('email',191)->unique();
		    $table->string('password');
		    $table->enum('role',['admin','phedc']);
		    $table->rememberToken();
		    $table->timestamps();
	    });

	    Schema::create('password_resets', function (Blueprint $table) {
		    $table->string('email',191)->index();
		    $table->string('token');
		    $table->timestamp('created_at')->nullable();
	    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('customers');
	    Schema::dropIfExists('recurrent');
	    Schema::dropIfExists('tips');
	    Schema::dropIfExists('customer_meters');
	    Schema::dropIfExists('transactions');
	    Schema::dropIfExists('cards');
	    Schema::dropIfExists('settings');
	    Schema::dropIfExists('coupons');
	    Schema::dropIfExists('payments');
	    Schema::dropIfExists('users');
	    Schema::dropIfExists('password_resets');
    }
}
